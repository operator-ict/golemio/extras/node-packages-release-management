# Node package release management

* Tool for developers for simpler package version bumping using Yarn and GitLab API
* Detailed description in the [`package-releases.ipynb`](./package-releases.ipynb)

## Run locally (can be faster):
* clone repository
* fill gitlab access token into `.env` file (see `.env-example` and [Gitlab docs](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html#create-a-personal-access-token))
* run `docker-compose up`
* open jupyter on http://localhost:8888
* open `package-releases.ipynb` notebook and follow its instructions


## Run on jupyter.golemio.cz

* clone repository
* fill gitlab access token into `.env` file (see `.env-example` and [Gitlab docs](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html#create-a-personal-access-token))
* setup environment by running `sh setup_venv.sh` in this directory
* Follow instructions in `package-release.ipynb`
