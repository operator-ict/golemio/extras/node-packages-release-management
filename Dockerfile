FROM jupyter/minimal-notebook:latest

ARG NB_USER=jovyan
ARG NB_UID=1000

USER root
RUN apt-get update && apt-get install --yes --no-install-recommends \
    dnsutils \
    git \
    iputils-ping \
    openssh-server \
    libproj-dev proj-data proj-bin libgeos-dev \
    netbase \
    curl \
    jq \
 && rm -rf /var/lib/apt/lists/*

USER $NB_UID
COPY requirements.txt /tmp/requirements.txt
RUN pip install --requirement /tmp/requirements.txt && \
    fix-permissions $CONDA_DIR && \
    fix-permissions /home/$NB_USER

WORKDIR /home/$NB_USER/work/gitlab
COPY gitlab_methods.py ./gitlab_methods.py
COPY package-releases.ipynb ./package-releases.ipynb
COPY readme.md ./readme.md

CMD jupyter lab

