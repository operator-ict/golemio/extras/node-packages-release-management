from shutil import rmtree
import os
import gitlab
import json
import subprocess
import datetime
import logging
import re
import filecmp
import shutil
import os
import tempfile

def get_project_ids_of_group(group_id, gl, include_archived = False):
    '''
    Returns list of ProjectIDs included in the GitLab Subgroup
    
    Parameters:
    `group_id`: int - subgroup ID (right below the subgroup name in Gitlab UI)
    `include_archived`: bool - whether to include archived projects
    
    returns List[int]
    '''
    if not include_archived:
        return [p.id for p in gl.groups.get(group_id).projects.list(all=True) if not p.archived]
    else:
        return [p.id for p in gl.groups.get(group_id).projects.list(all=True)]

def load_projects(project_ids,gl):
    '''
    Return a dict with GitLab projects objects with names as keys
    
    Parameters: 
    `project_ids`: list[int] list of project ids
    '''
    projects={}
    for project_id in project_ids:
        p = gl.projects.get(project_id)
        projects[p.name] = p
        #logging.info(f'Loading project `{p.name}`')
    return projects

def create_merge_request(project, source_branch, target_branch, message, remove_source_branch, squash = False):
    '''
    Creates a GitLab Merge request from `source_branch` to `target_branch`.
    
    Returns a MR-object even if it already exists.
    
    Parameters: 
    `project`: GitLab project object
    `source_branch`: str - source branch name
    `target_branch`: str - target branch name
    `message`: str - MR message
    `squash`: bool - whether to squash commits or not
    '''
    try:
        return project.mergerequests.create({'source_branch': source_branch,
                                           'target_branch': target_branch,
                                           'title': message,
                                           'remove_source_branch': remove_source_branch,
                                           'squash': squash
                                          })
    except gitlab.GitlabCreateError as e:
        logging.warning(e)
        return _gitlab_get_merge_request(project, source_branch, target_branch)
    
    
def get_commit_difference(project, source_branch, target_branch):
    '''
    Returns a number of commits between `target_branch` and `source_branch` in `project`

    Parameters: 
    `project`: GitLab project object
    `source_branch`: str - source branch name
    `target_branch`: str - target branch name
    '''
    return len(project.repository_compare(target_branch, source_branch)['commits'])

def bump_dependencies_and_create_mr(project, branch, remove_source_branch, file_names = ['package.json', 'yarn.lock'], new_branch_name = 'dependencies/auto_upgrade'):
    '''
    Get dependency files and perfrom yarn upgrades. Check if the `package.json` and `yarn.lock` get changed and if they do, try to create new branch, commit new dependency files and create merge request
    '''
    with tempfile.TemporaryDirectory(dir='.') as tmp_dir:
        _gitlab_get_files(project, branch, file_names, tmp_dir)
        logging.info(f'upgrading dependencies in {project.name}/{branch}')
        _node_upgrade_dependencies(tmp_dir)
        if not _are_files_different(file_names,tmp_dir):
            return None
        try:
            _gitlab_create_new_branch(project, branch, new_branch_name)
        except GitlabBranchAlreadyExists:
            pass
        message = 'chore: bump golemio dependecies'
        _gitlab_commit_updated_files(project, new_branch_name, file_names, message, tmp_dir)    
        return create_merge_request(project, new_branch_name, branch, message, remove_source_branch, squash=True)




def _gitlab_get_merge_request(project, source_branch, target_branch):
    '''
    Returns MR between `target_branch` and `source_branch` in `project` if it exists. Return None if it does not exist.

    Parameters: 
    `project`: GitLab project object
    `source_branch`: str - source branch name
    `target_branch`: str - target branch name
    '''

    for mr in project.mergerequests.list():
        if mr.source_branch == source_branch and mr.target_branch == target_branch:
            return mr
    return None

def bump_package_and_create_mr(project, branch, remove_source_branch):
    '''
    
    '''
    with tempfile.TemporaryDirectory(dir='.') as tmp_dir:
        _gitlab_get_files(project, branch, ['package.json'], tmp_dir)
        _node_bump_package_version(tmp_dir)
        new_package_version = _node_get_package_version(tmp_dir)
        new_branch_name = f'bump/{new_package_version}'

        try:
            logging.info("Creating new branch")
            _gitlab_create_new_branch(project, branch, new_branch_name)
            logging.info("New branch was created")
        except GitlabBranchAlreadyExists as ex:
            logging.warning(ex)
        logging.info("Compare changes in package.json")
        diff = _gitlab_commit_updated_files(project, new_branch_name, ['package.json'], f'chore: bump version to {new_package_version}',tmp_dir)    
        if not diff:
            logging.info("No difference")
            return None
        logging.info("Creating MR")
        return create_merge_request(project, new_branch_name, branch, f'Bump {new_branch_name} -> {branch}', remove_source_branch)


def _gitlab_get_files(project, branch, file_names, tmp_dir):
    logging.info(f'Getting {", ".join(file_names)} in {project.name}/{branch}')
    for file_name in file_names:
        backup_file_name = f'{tmp_dir}/{file_name}.backup'
        local_file_path = f'{tmp_dir}/{file_name}'
        with open(local_file_path, "wb") as local_file:
            project.files.raw(file_path=file_name, ref=branch, streamed=True, action=local_file.write)
        shutil.copy2(local_file_path, backup_file_name)
            
def _node_bump_package_version(tmp_dir):
    _run_yarn(['yarn', 'config', 'set', 'version-git-tag', 'false'],cwd=tmp_dir)
    _run_yarn(['yarn', 'version', '--patch'],cwd=tmp_dir)

def _node_get_package_version(cwd):
    return subprocess.run(['npm run env | grep npm_package_version | cut -d \'=\' -f 2 '], shell=True, capture_output=True,cwd=cwd).stdout.decode().strip()    


def _gitlab_create_new_branch(project, ref, new_branch_name):
    try:
        project.branches.get(new_branch_name)
        raise GitlabBranchAlreadyExists(f'Branch \'{new_branch_name}\' already exists')
    except gitlab.GitlabGetError:
        project.branches.create({'branch': new_branch_name, 'ref': ref})

        
def _run_yarn(command_arr,cwd=None):
    output = subprocess.run(command_arr, capture_output=True,cwd=cwd)
    #logging.debug(f'stderr: {output.stderr.decode()}')

    
def _gitlab_commit_updated_files(project, branch, file_names, message, tmp_dir):
    actions = []
    
    ### ALREADY CHECKED IN THE CONTEXT OF bump_dependencies_and_create_mr
    #if not _is_files_different (file_names):
    #    logger.debug('No files to upload')
    #    return False
    for file_name in file_names:
        with open(f'{tmp_dir}/{file_name}', "r") as local_file:
            actions.append({
                              "action": "update",
                              "file_path": file_name,
                              "content": local_file.read()
                            })
    remote_data = {
        'branch': branch,
        'commit_message': message,
        'actions': actions
    }
    logging.info(f'Commiting files {[f for f in file_names]} into branch \'{branch}\' on project \'{project.name}\' ')
    commit = project.commits.create(remote_data)
    return True


def _node_upgrade_dependencies(tmp_dir):
    logging.debug("Starting dependency upgrade")
    #_run_yarn(['yarn', 'upgrade'], tmp_dir)    
    _run_yarn(['yarn', 'upgrade', '--latest', '--scope', '@golemio'], tmp_dir)
    logging.info("Dependency upgrade is done")
    
def _are_files_different(file_names,tmp_dir)-> bool:
    for f in file_names:
        if not filecmp.cmp(f'{tmp_dir}/{f}', f'{tmp_dir}/{f}.backup', shallow=False):
            return True
    return False

    
class GitlabBranchAlreadyExists(Exception):
    pass

